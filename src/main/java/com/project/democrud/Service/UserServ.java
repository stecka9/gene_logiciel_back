package com.project.democrud.Service;

import com.project.democrud.UserRepo.UserConnRe;
import com.project.democrud.UserRepo.UserRepo;
import com.project.democrud.entity.ERole;
import com.project.democrud.entity.User;
import com.project.democrud.entity.UserConn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Objects;
import java.util.Optional;

@Service
public class UserServ {

    private final UserConnRe userRep;
    private final UserRepo userRepo;
    @Autowired
    public UserServ(UserConnRe userRep,UserRepo userRepo){
        this.userRep = userRep;
        this.userRepo = userRepo;
    }
    public UserConn SignIN(String login, String passwd){

        if(userRep.getUserByLoginAndPasswd(login, passwd) != null){
            return userRep.getUserByLoginAndPasswd(login, passwd);
        }
        else {
            return null;
        }
    }

    @Transactional
    public void UpdatePasswd(String login, String newpasswd){

        UserConn user = this.userRep.findUserByLogin(login);
        User user1 = this.userRepo.findUserByImm(login);
        if(user == null){
            throw new IllegalStateException("No user with this login");
        }
        if(!Objects.equals(newpasswd, user.getPasswd())) {
            user.setPasswd(newpasswd);
            user.setChangepass("TRUE");
            user1.setMot_de_pass(newpasswd);

        }

        else {
            throw new IllegalStateException("It is the old password");
        }

    }

}
