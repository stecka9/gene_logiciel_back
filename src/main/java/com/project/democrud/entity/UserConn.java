package com.project.democrud.entity;

import javax.persistence.*;

@Entity
@Table(name="userConn")
public class UserConn{
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Integer id;
    @Column
    private String login;
    @Column
    private String passwd;
    @Column
    private ERole role;
    @Column
    private  String changepass;
    public UserConn( String login, String passwd, ERole role,String change) {
        this.login = login;
        this.passwd = passwd;
        this.role = role;
        this.changepass=change;
    }

    public UserConn() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public ERole getRole() {
        return role;
    }

    public void setRole(ERole role) {
        this.role = role;
    }

    public String getChangepass() {
        return changepass;
    }

    public void setChangepass(String change) {
        this.changepass = change;
    }
}

