package com.project.democrud.entity;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_AGENT

}
