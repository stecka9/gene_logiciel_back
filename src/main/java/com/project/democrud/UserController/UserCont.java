package com.project.democrud.UserController;

import com.project.democrud.MessageResponse;
import com.project.democrud.Service.UserServ;
import com.project.democrud.UserRepo.UserConnRe;
import com.project.democrud.entity.ERole;
import com.project.democrud.entity.UserConn;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@CrossOrigin("*")
@RestController
@RequestMapping(path="api/v1/user")

public class UserCont {

    private final UserServ userServ;
    private final UserConnRe userRep;
    @Autowired
    public UserCont(UserServ userServ, UserConnRe userRep){

        this.userServ = userServ;
        this.userRep = userRep;
    }

    @PostMapping("/SignIn")
    public ResponseEntity<?> SignIn(@RequestBody Map<String, String> request){

        String login = request.get("login");
        String passwd = request.get("passwd");
        UserConn response = this.userServ.SignIN(login, passwd);
        if(response == null){
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(new MessageResponse("bad credentials"));
        }
        else{
            return ResponseEntity.ok(response);
        }
    }
    @PutMapping("/SignIn/UpdatePasswd")
    public ResponseEntity<?> UpdatePasswd(@RequestBody Map<String, String> request){

        String login = request.get("login");
        String newpasswd = request.get("newpasswd");
        this.userServ.UpdatePasswd(login,newpasswd);
        return ResponseEntity.ok(new MessageResponse("Password modified successfully!"));
    }
}
