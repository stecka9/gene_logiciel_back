package com.project.democrud.UserRepo;

import com.project.democrud.entity.UserConn;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Repository;

import java.util.Optional;
@EnableJpaRepositories
@Repository
public interface UserConnRe extends JpaRepository<UserConn, Integer> {

    @Query("SELECT u FROM UserConn u WHERE u.login = ?1 AND u.passwd = ?2")
    Optional<UserConn> findUserByLoginAndPasswd(String login, String passwd) ;

    @Query("SELECT u FROM UserConn u WHERE u.login = ?1 AND u.passwd = ?2")
    UserConn getUserByLoginAndPasswd(String login, String passwd);

    @Query("SELECT u FROM UserConn u WHERE u.login = ?1")
    UserConn findUserByLogin(String login);

}

