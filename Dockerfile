FROM openjdk:19
WORKDIR /app
ADD target/democrud-0.0.1-SNAPSHOT.jar ./democrud-0.0.1-SNAPSHOT.jar
EXPOSE 8084
ENTRYPOINT ["java","-jar","democrud-0.0.1-SNAPSHOT.jar"]